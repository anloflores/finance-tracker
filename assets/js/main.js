var ct_debit = document.getElementById("ct-debit");
var ct_expense = document.getElementById("ct-expense");
var ct_trigger = document.getElementById("ct-trigger");

var ct_next_card = document.querySelector(".next-card");
var ct_prev_card = document.querySelector(".prev-card");

var add_transaction_btn = document.querySelector("#add-transaction-btn");

var closes = document.querySelectorAll(".close-btn");

var _curr_card = 0;
var _curr_option = 1;
var _curr_card_max = document.querySelectorAll(".card").length;
var _data = {
  cards: [],
  transactions: [],
};

if (localStorage.getItem("data") !== null) {
  _data = JSON.parse(localStorage.getItem("data"));
}



_render_weeks("debit");
_render_weeks("expense");
_render_card();
_render_transactions();
_get_overall_total();

function _get_overall_total() {
  var sum = 0;
  for (var t of _data.transactions) {
    for (var c of t) {
      if (c.type == 1) sum += parseFloat(c.amount);
      if (c.type == 0) sum -= parseFloat(c.amount);
    }
  }

  document.getElementById("total-savings").innerHTML = sum;
}

ct_debit.addEventListener("click", () => _activate_debit(ct_debit));
ct_expense.addEventListener("click", () => _activate_expense(ct_expense));

ct_next_card.addEventListener("click", () => _select_card(1));
ct_prev_card.addEventListener("click", () => _select_card(0));

for (var c of closes) {
  c.addEventListener("click", () => _close_panel());
}

function _close_panel() {
  document.querySelector(".action-panels.active").classList.remove("active");
  document.querySelector(".action-panel.active").classList.remove("active");
}

/* Menu */
document
  .getElementById("add-transaction-menu")
  .addEventListener("click", () => {
    document.querySelector(".action-panels").classList.add("active");
    document
      .querySelector(".action-panel.add-transaction")
      .classList.add("active");
  });

document.getElementById("add-card-menu").addEventListener("click", () => {
  document.querySelector(".action-panels").classList.add("active");
  document.querySelector(".action-panel.add-card").classList.add("active");
});

document.getElementById("transactions-menu").addEventListener("click", () => {
  document.querySelector(".action-panels").classList.add("active");
  document
    .querySelector(".action-panel.view-transactions")
    .classList.add("active");
});

/* Transaction Starts*/

add_transaction_btn.addEventListener("click", () => _add_transaction());
var amount_input = document.querySelector(".add-amount-input");
document.querySelector(".type-debit").addEventListener("click", () => {
  amount_input.style.color = "#56F46E";
  document.querySelector(".add-type").value = 1;
});
document.querySelector(".type-expense").addEventListener("click", () => {
  amount_input.style.color = "#DD4C4C";
  document.querySelector(".add-type").value = 0;
});
function _add_transaction() {
  // form
  var type = document.querySelector(".add-type").value;
  var amount = document.querySelector(".add-amount-input").value;
  var date = document.querySelector(".add-calendar").value;
  var comment = document.querySelector(".add-comment").value;

  if (type == "" || amount == "" || date == "" || comment == "") {
    alert("All Fields are required");
    return;
  }

  _data.transactions[_curr_card].push({
    type: type,
    amount: amount,
    date: date,
    comment: comment,
  });

  localStorage.setItem("data", JSON.stringify(_data));
  closes[0].click();
  alert("Successfully Recorded");
  _render_transactions();
  _render_weeks("debit");
  _render_weeks("expense");
  _get_overall_total();
  return true;
}
/* Transaction Ends*/

/* Add Card Start */
document.getElementById("add-card-btn").addEventListener("click", () => {
  // form
  var name = document.querySelector(".add-card-bank").value;
  var color = document.querySelector(".add-card-color").value;

  if (name == "" || color == "") {
    alert("All Fields are required");
    return;
  }

  _data.cards.push({
    name: name,
    color: color,
  });

  _data.transactions[_data.cards.length - 1] = [];

  localStorage.setItem("data", JSON.stringify(_data));
  closes[0].click();
  alert("Successfully Recorded");
  _render_card();
  return true;
});
/* Add Card End */

function _select_card(dir) {
  if (dir == 1) {
    if (_curr_card >= _curr_card_max - 1) return;
    _curr_card++;
  }
  if (dir == 0) {
    if (_curr_card == 0) return;
    _curr_card--;
  }

  var prev_card = document.querySelector(".card.active");
  prev_card.classList.remove("active");
  prev_card.classList.add("inactive");

  var next_card = document.querySelector(".card.card-" + _curr_card);
  next_card.classList.add("active");
  next_card.classList.remove("inactive");

  _render_transactions();
  _render_weeks("debit");
  _render_weeks("expense");
}

function _activate_debit(e) {
  var active = document.querySelector(".report-control-option.active");
  active.classList.remove("active");
  e.classList.add("active");
  _move_trigger("left");
}

function _activate_expense(e) {
  var active = document.querySelector(".report-control-option.active");
  active.classList.remove("active");
  e.classList.add("active");
  _move_trigger("right");
}

function _move_trigger(dir) {
  var wk_holder_debit = document.querySelector(".week-holder.debit");
  var wk_holder_expense = document.querySelector(".week-holder.expense");
  if (dir == "right") {
    ct_trigger.style.marginLeft = "110px";
    wk_holder_debit.classList.remove("active");
    wk_holder_expense.classList.add("active");
  } else {
    ct_trigger.style.marginLeft = "-20px";
    wk_holder_debit.classList.add("active");
    wk_holder_expense.classList.remove("active");
  }
}

function _render_weeks(type) {
  var wk_holder = document.querySelector(".week-holder." + type + " > .list");
  var days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
  var debit = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
  };
  var expense = {
    0: 0,
    1: 0,
    2: 0,
    3: 0,
    4: 0,
    5: 0,
    6: 0,
  };
  var total_debit = 0;
  var total_expense = 0;

  var now = new Date();
  var wk = now.getWeek();

  if (_data.transactions[_curr_card] !== undefined) {
    for (var v of _data.transactions[_curr_card]) {
      var d = new Date(v.date);

      if (!isNaN(d.getDay()) && d.getWeek() == wk) {
        if (v.type == 1) {
          total_debit += parseFloat(v.amount);
          debit[d.getDay()] += parseFloat(v.amount);
        }

        if (v.type == 0) {
          total_expense += parseFloat(v.amount);
          expense[d.getDay()] += parseFloat(v.amount);
        }
      }
    }
  }

  var data = null;
  var total = 0;
  if (type == "debit") {
    data = debit;
    total = total_debit;
  }

  if (type == "expense") {
    data = expense;
    total = total_expense;
  }

  var i = 0;
  wk_holder.innerHTML = "";
  for (var day of days) {
    var percentage = (data[i] / total) * 100;
    wk_holder.innerHTML +=
      `
            <div class="week ` +
      type +
      `">
                <div class="week-day">
                    ` +
      day +
      `
                </div>
                <div class="week-graph-holder">
                    <span class="week-graph-holder-label">
                        ${data[i]}
                    </span>

                    <div class="week-graph-holder-line" style="width: ` +
      percentage +
      `%;"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        `;

    i++;
  }

  document.getElementById("total-" + type).textContent = total;
}

function _render_card() {
  var i = 0;
  document.querySelector(".card-container").innerHTML = "";
  for (var card of _data.cards) {
    document.querySelector(".card-container").innerHTML += `
            <div class="card ${
              i == 0 ? "active" : "inactive"
            } card-${i}" style="background-color: ${card.color};">
                    <div class="card-header">${card.name}</div>
                    <div class="card-info">
                        <div class="card-no f-left">--- ---- ----</div>
                        <div class="card-exp f-right">--/--</div>
                        <div class="clearfix"></div>
                        <div class="card-name">------ ------</div>
                    </div>
                </div>
        `;

    i++;
  }
  _curr_card = 0;
  _curr_card_max = document.querySelectorAll(".card").length;
}

function _render_transactions() {
  document.querySelector(".transaction-holder").innerHTML = "";
  if (_data.transactions[_curr_card] !== undefined) {
    for (var t of _data.transactions[_curr_card]) {
      document.querySelector(".transaction-holder").innerHTML += `
                <div class="item ${t.type == 1 ? "debit" : "expense"} ">
                    <div class="date">${t.date}</div>
                    <div class="amount">${t.amount}</div>
                    <div class="description">${t.comment}</div>
                </div>
            `;
    }
  }
}
