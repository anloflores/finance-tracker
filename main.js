const { app, BrowserWindow } = require("electron");

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    resizable: false,
    width: 375,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // win.webContents.openDevTools();

  // and load the index.html of the app.
  win.loadFile("index.html");
}

app.whenReady().then(createWindow);
